package com.cathay.ai.aa.a0.vo;

public class AAA0_0100_vo {

private String data;
private String returnCode;
private String returnMess;


public String getData() {
	return data;
}


public void setData(String data) {
	this.data = data;
}


public String getReturnCode() {
	return returnCode;
}


public void setReturnCode(String returnCode) {
	this.returnCode = returnCode;
}


public String getReturnMess() {
	return returnMess;
}


public void setReturnMess(String returnMess) {
	this.returnMess = returnMess;
}

//@param data Return data json
//@param returnCode: 0 => complete -1=> error
public AAA0_0100_vo() {
	
}

}
