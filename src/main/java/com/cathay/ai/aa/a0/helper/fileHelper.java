package com.cathay.ai.aa.a0.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;

import com.cathay.ai.qr.config.ConfigManager;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.util.StringUtils;

public class fileHelper {
	private static String PathUpload = "/uploads";
	private static Logger Log = Logger.getLogger(fileHelper.class);
	private static String osName = ObjectUtils.toString(System.getProperties().get("os.name"));
	// private static String INSERT_UPLOADFILE =
	// "com.dano.web.app.aa.b1.modun.AAB1_0100_INSERT_UPLOAD_FILE";

	public static Map saveFile(String Path, MultipartFile[] file) throws IOException {
		
		if (osName.contains("Windows")) {
			PathUpload = "/uploads";
		} else {
			PathUpload = "/var/www/webdano/public/uploads";
		}
		Map map = new HashMap();
		Path uploadPath = Paths.get(PathUpload);

		if (!Files.exists(uploadPath)) {
			Files.createDirectories(uploadPath);
		}
		if (!"".equals(Path)) {
			uploadPath = Paths.get(Path);
		}
		for (int i = 0; i < file.length; i++) {
			Map info = new HashMap();
			String fileName = StringUtils.cleanPath(file[i].getOriginalFilename());
			long size = file[i].getSize();
			String filecode;
			String path = "";
			MultipartFile multipartFile = file[i];
			try (InputStream inputStream = multipartFile.getInputStream()) {
				Path filePath = uploadPath.resolve(fileName);
				Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
				map.put("FILE_NAME", ObjectUtils.toString(fileName));
				map.put("FILE_SIZE", "0");
				map.put("FILE_PATH", ObjectUtils.toString(filePath));
				map.put("NAME_MODIFY", "null");
				map.put("AUTHOR", "null");
				map.put("IP_UPLOAD", "null");
				map.put("IS_SHOW", "1");
				map.put("returnCode", "0");
				map.put("DownloadUri", "/downloadFile/" + fileName);
				System.out.println("--------------------------------");
				System.out.println("UPLOAD FILE: [COMPLETE!!]");
				System.out.println("--------------------------------");
			} catch (IOException ioe) {
				map.put("returnCode", "-1");
				map.put("returnMess", ioe.getCause());
				System.out.println("--------------------------------");
				System.out.println("UPLOAD FILE: [Failed!!]");
				System.out.println("--------------------------------");
			}

		}
		// String fileCode = RandomStringUtils.randomAlphanumeric(8);

		return map;
	}

	private Path foundFile;

	public Resource getFileAsResource(String fileCode) throws IOException {
		String osName = ObjectUtils.toString(System.getProperties().get("os.name"));
		if (osName.contains("Windows")) {
			PathUpload = "/uploads";
		} else {
			PathUpload = "/var/www/webdano/public/uploads";
		}
		Path dirPath = Paths.get(PathUpload);

		Files.list(dirPath).forEach(file -> {
			if (file.getFileName().toString().startsWith(fileCode)) {
				foundFile = file;
				return;
			}
		});

		if (foundFile != null) {
			return new UrlResource(foundFile.toUri());
		}

		return null;
	}

	public Resource getFileAsResource(String path, String fileName) throws IOException {
		String osName = ObjectUtils.toString(System.getProperties().get("os.name"));
		if (osName.contains("Windows")) {
			PathUpload = "/uploads";
		} else {
			PathUpload = "/var/www/webdano/public/uploads";
		}
		Path dirPath = Paths.get(path);
		if ("".equals(path)) {
			dirPath = Paths.get(PathUpload);
		}

		Files.list(dirPath).forEach(file -> {
			if (file.getFileName().toString().startsWith(fileName)) {
				foundFile = file;
				return;
			}
		});

		if (foundFile != null) {
			return new UrlResource(foundFile.toUri());
		}

		return null;
	}

	public static void createLog4jProperties() {
		Properties properties = new Properties();
		FileOutputStream fileOutputStream;
		try {
			
			if (osName.contains("Windows")) {
				fileOutputStream = new FileOutputStream(new File(".").getCanonicalPath() + "/log4j.properties");
			} else {
				fileOutputStream = new FileOutputStream("/opt/tomcat/webapps/JavaQrCode/WEB-INF/log4j.properties");
			}

			properties.setProperty("log4j.rootCategory", "DEBUG, all");
			properties.setProperty("log4j.appender.all", "org.apache.log4j.ConsoleAppender");
			properties.setProperty("log4j.appender.all.layout", "org.apache.log4j.PatternLayout");
			properties.setProperty("log4j.appender.all.layout.ConversionPattern",
					"%d{yyyy-MM-dd HH:mm:ss,SSS} [%-5p][%C{1}.%M()(%L)] - %m%n");
			 properties.store(fileOutputStream,"Author: "+fileHelper.class.getSimpleName());
			  
			        fileOutputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static Properties getPropertiesFile(String fileName) {
		File file;
		Properties props = null;
		try {
			file = new File(new File(".").getCanonicalPath() + "/" + fileName);
			props.load(new FileInputStream(file));
			Log.debug("Properties File Location: [" + file.getAbsolutePath() + "]");
		} catch (IOException e) {
			Log.error("IOException: " + e.getCause());
			Log.error(e);
		}
		return props;
	}

	public List getListOfFiles(String Path) {
		List<Map> li = new ArrayList();

		File directoryPath = new File(Path);
		String contents[] = directoryPath.list();
		for (int i = 0; i < contents.length; i++) {
			Map item = new HashMap();
			item.put("NAME", contents[i]);
			File file = new File(Path + "/" + contents[i]);

			System.out.println(file.getParent());
			item.put("ROOT", file.getAbsoluteFile());
			item.put("PREROOT", file.getParentFile().getParent());
			if (ObjectUtils.toString(file.getName()).indexOf(".") == -1) {
				item.put("TYPE", 0);
			} else {
				item.put("TYPE", 1);
			}

			li.add(item);
		}
		return li;

	}

	public static String getCanonicalPath() throws IOException {
		return new File(".").getCanonicalPath();
	}

	public static void main(String args[]) throws IOException {
		fileHelper fi = new fileHelper();
		System.out.println(fi.getListOfFiles("D:/"));
	}
}
