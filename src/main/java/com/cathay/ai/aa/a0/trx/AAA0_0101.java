package com.cathay.ai.aa.a0.trx;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.cathay.ai.aa.a0.modun.AAA0_0100_mod;
import com.cathay.ai.aa.a0.modun.AAA0_0101_mod;
import com.cathay.ai.aa.a0.vo.AAA0_0100_vo;
import com.darkprograms.speech.translator.GoogleTranslate;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.EncodeHintType;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

@Controller
public class AAA0_0101 {
	private String osName = ObjectUtils.toString(System.getProperties().get("os.name"));
	private String javaVersion = ObjectUtils.toString(System.getProperties().get("java.version"));
	private String SYSID = this.getClass().getSimpleName();
	ObjectMapper objectMapper = new ObjectMapper();
	
	@RequestMapping(value = "/barcodetiff", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String barcodetiff(@RequestParam("file") MultipartFile file[], @RequestParam Map<String, Object> params, HttpServletResponse rep,HttpServletRequest req) throws JsonProcessingException {
		List<Map> vo = new ArrayList();
		try {
			for (int i = 0; i < file.length; i++) {
				BufferedImage image =null;
				//image = ImageIO.read();
				vo=AAA0_0101_mod.getBarcode(file[i].getInputStream(), 15, 2, null);
			}
		} catch (Exception e) {
			
		}
		return objectMapper.writeValueAsString(vo);
	}
	@RequestMapping(value = "/translate", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String dotranslate(@RequestParam Map<String, Object> params, HttpServletResponse rep,HttpServletRequest req) throws JsonProcessingException {
		List<Map> vo = new ArrayList();
		Map map = new HashMap();
		try {
			String langueTranslate=ObjectUtils.toString("language","");
			String text=ObjectUtils.toString(params.get("text"),"");
			String result="";
			if(!"".equals(langueTranslate)) {
				result = GoogleTranslate.translate(langueTranslate, text);
			}else {
				
				result = GoogleTranslate.translate("en", text);
			}
			map.put("returnCode", 0);
			map.put("data", result);
			map.put("mess", "Dịch thuật thành công!");
			
		
		} catch (Exception e) {
			map.put("returnCode", -1);
			map.put("data", null);
			map.put("mess", "Dịch thuật thất bại!");
			e.printStackTrace();
		}
		vo.add(map);
		return objectMapper.writeValueAsString(vo);
	}
	
	
	public static void main(String[] args) {
	System.out.println("==========================================================================");
  AAA0_0101_mod mod= new AAA0_0101_mod();
  String qrCodeData = "Hello World!";
  String filePath = "QRCode.png";
  String charset = "UTF-8"; // or "ISO-8859-1"

	
	try 
		    { 
		        Process p=Runtime.getRuntime().exec("C:/ZBar/bin/zbarimg  D:/Z.PNG"); 
		        p.waitFor(); 
		        System.out.println("Process: ["+p+"]");
		        BufferedReader reader=new BufferedReader(
		            new InputStreamReader(p.getInputStream())
		        ); 
		        String line=reader.readLine(); 
		        while(line!=null) 
		        { 
		            System.out.println(line); 
		            line=reader.readLine(); 
		        } 

		    }
		    catch(IOException e1) {
		    	System.out.println(e1.getCause());
		    } 
		    catch(InterruptedException e2) {
		    	System.out.println(e2.getCause());
		    } 
		    System.out.println("==========================================================================");
		   
		}
	
	}


