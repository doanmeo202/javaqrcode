package com.cathay.ai.aa.a0.trx;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.cathay.ai.aa.a0.helper.fileHelper;
import com.cathay.ai.aa.a0.modun.AAA0_0100_mod;
import com.cathay.ai.aa.a0.vo.AAA0_0100_vo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.AnyGetterWriter;
import boofcv.io.UtilIO;
import boofcv.io.image.UtilImageIO;

@Controller
public class AAA0_0100 {
	private String osName = ObjectUtils.toString(System.getProperties().get("os.name"));
	private String javaVersion = ObjectUtils.toString(System.getProperties().get("java.version"));
	private String SYSID = this.getClass().getSimpleName();
	private static final Logger log = Logger.getLogger(AAA0_0100.class);
	String pathSYSID = "AA/A0/AAA0_0100/";
	ObjectMapper objectMapper = new ObjectMapper();

	@RequestMapping(value = "/AAA0_0100/checkLibJava", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String checkJavaLibPath(Model model, HttpServletRequest req) throws JsonProcessingException {
		AAA0_0100_vo vo = new AAA0_0100_vo();
		String path = System.getProperty("java.library.path");
		List<String> paths = java.util.List.of(path.split(";"));
		Map returnjson = new HashMap();
		log.debug("====================================");

		int i = 0;
		for (String itemm : paths) {
			i++;
			log.debug("Java.library.path: [" + itemm + "]");
			returnjson.put("Java.library.path " + i, "[" + itemm + "]");
		}
		log.debug("====================================");
		ObjectMapper objectMapper = new ObjectMapper();
		// rep.addHeader("Access-Control-Allow-Origin", "*");
		return objectMapper.writeValueAsString(returnjson);

	}

	@RequestMapping(value = "/livePrompt")
	public String doPromptLiveDemo(@RequestParam Map<String, Object> params, HttpServletResponse rep,
			HttpServletRequest req) throws JsonProcessingException {
		return pathSYSID + "AAA0_0100_liveDemo";
	}

	@RequestMapping(value = "/scanQrPrompt")
	public String doScanQrPrompt(@RequestParam Map<String, Object> params, HttpServletResponse rep,
			HttpServletRequest req) throws JsonProcessingException {
		return pathSYSID + "AAA0_0100_ScanQr";
	}

	@RequestMapping(value = "/scanQrPrompt2")
	public String doScanQrPrompt2(@RequestParam Map<String, Object> params, HttpServletResponse rep,
			HttpServletRequest req) throws JsonProcessingException {
		return pathSYSID + "AAA0_0100_ScanQr2";
	}

	//@RequestMapping(value = "/qrcode", produces = "text/plain;charset=UTF-8")
	
	@RequestMapping(value = "/qrcode", method = RequestMethod.POST, consumes = {"multipart/form-data"}, produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String doApi(@RequestParam("file") MultipartFile file[], @RequestParam Map<String, Object> params,
			HttpServletResponse rep, HttpServletRequest req) throws JsonProcessingException {
		log.debug("==============================================");
		log.debug("Class      : [" + SYSID + "]");
		log.debug("RequestURI : [" + req.getRequestURI() + "]");
		log.debug("osName     : [" + osName + "]");
		log.debug("javaVersion: [" + javaVersion + "]");
		log.debug("file length: [" + file.length+"]");
		if(file.length>0) {
			log.debug("file content: [" + file[0].getContentType()+"]");
			log.debug("file size   : [" + file[0].getSize()+"]");
		}
			log.debug("----------------------------------------------");
		AAA0_0100_vo voOut = new AAA0_0100_vo();
		List<AAA0_0100_vo> livo = new ArrayList();
		if(file.length<1) {
			voOut.setReturnCode("-1");
			voOut.setReturnMess("Không nhận được file gửi đến!");
			voOut.setData(null);
			setHeaderJson(rep);
			return objectMapper.writeValueAsString(voOut); 
		}
		try {
			for (int i = 0; i < file.length; i++) {
				BufferedImage image = null;
				image = ImageIO.read(file[i].getInputStream());

				AAA0_0100_vo vo = new AAA0_0100_vo();
				vo = AAA0_0100_mod.detectQRCode(image);
				AAA0_0100_mod mod= new AAA0_0100_mod();
		        mod.writeImage(image);
				livo.add(vo);
			}
		} catch (Exception e) {
			voOut.setReturnCode("-1");
			voOut.setReturnMess("Vui lòng kiểm tra lại file hình ảnh!");
			voOut.setData(null);
			log.error(e);
		}
		voOut.setData(objectMapper.writeValueAsString(livo));
		voOut.setReturnCode("0");
		log.debug("==============================================");
		setHeaderJson(rep);
		return objectMapper.writeValueAsString(voOut);
	}

	@RequestMapping(value = "/qrcodeBase64", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String doApiBase64(@RequestParam Map<String, Object> params, HttpServletResponse rep, HttpServletRequest req)
			throws JsonProcessingException {
		log.debug("==============================================");
		log.debug("Class      : [" + SYSID + "]");
		log.debug("RequestURI : [" + req.getRequestURI() + "]");
		log.debug("osName     : [" + osName + "]");
		log.debug("javaVersion: [" + javaVersion + "]");
		log.debug("----------------------------------------------");
		AAA0_0100_vo voOut = new AAA0_0100_vo();
		List<AAA0_0100_vo> livo = new ArrayList();

		BufferedImage image = base64ToImage(params);
		AAA0_0100_vo vo = new AAA0_0100_vo();
		vo = AAA0_0100_mod.detectQRCode(image);
		livo.add(vo);
		voOut.setData(objectMapper.writeValueAsString(livo));
		voOut.setReturnCode("0");
		log.debug("==============================================");
		setHeaderJson(rep);
		return objectMapper.writeValueAsString(voOut);
	}
	
	@RequestMapping(value = "/base64ToImage", produces = "text/plain;charset=UTF-8")
	@ResponseBody
	public String dobase64ToImage(@RequestParam Map<String, Object> params, HttpServletResponse rep,
			HttpServletRequest req) throws JsonProcessingException {
		log.debug("==============================================");
		log.debug("Class      : [" + SYSID + "]");
		log.debug("RequestURI : [" + req.getRequestURI() + "]");
		log.debug("osName     : [" + osName + "]");
		log.debug("javaVersion: [" + javaVersion + "]");
		log.debug("----------------------------------------------");
		AAA0_0100_vo voOut = new AAA0_0100_vo();
		List<AAA0_0100_vo> livo = new ArrayList();

		BufferedImage image = base64ToImage(params);
		AAA0_0100_vo vo = new AAA0_0100_vo();

		try {
			
			  AAA0_0100_mod mod= new AAA0_0100_mod();
	           mod.writeImage(image);
			vo.setData("OK");
			vo.setReturnCode("0");
		} catch (IOException e) {
			vo.setData("FAILED!!!");
			vo.setReturnCode("-1");
			e.printStackTrace();
		}
		livo.add(vo);
		voOut.setData(objectMapper.writeValueAsString(livo));
		voOut.setReturnCode("0");
		log.debug("==============================================");
		setHeaderJson(rep);
		return objectMapper.writeValueAsString(voOut);
	}

	public HttpServletResponse setHeaderJson(HttpServletResponse rep) {
		rep.addHeader("Access-Control-Allow-Origin", "*");
		rep.setHeader("Access-Control-Allow-Origin", "*");
		rep.setContentType("application/json");
		rep.setCharacterEncoding("UTF-8");
		return rep;
	}

	public static BufferedImage base64ToImage(Map params) {
		log.debug("----------------------------------------------");
		String sourceData = ObjectUtils.toString(params.get("imageString"), "");
		String imageString;
		BufferedImage image = null;
		String temp = "data:image";
		if (sourceData.startsWith(temp)) {
			log.debug("imageString => [data:image/type;base64]");
			String[] parts = sourceData.split(",");
			imageString = parts[1];
		} else {
			imageString = ObjectUtils.toString(params.get("imageString"));
			log.debug("imageString => [base64]");
		}
		byte[] decodedBytes = Base64.getDecoder().decode(imageString);
		ByteArrayInputStream bais = new ByteArrayInputStream(decodedBytes);
		try {
			image = ImageIO.read(bais);
			AAA0_0100_mod mod= new AAA0_0100_mod();
	        mod.writeImage(image);
			// File outputfile = new File("D:/Test.png");
			// ImageIO.write(image, "png", outputfile);
		} catch (IOException e) {
			log.error("IOException: [" + e.getCause() + "]");
		}

		log.debug("----------------------------------------------");
		return image;
	}

}
