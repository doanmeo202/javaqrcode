package com.cathay.ai.aa.a0.Test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.commons.lang3.ObjectUtils;



public class Test {
public static void main(String[] args) throws IOException {
	try {
		translate();
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
public static void translate() throws IOException  {
	 Properties properties = new Properties();
     FileOutputStream fileOutputStream
         = new FileOutputStream(
             "log4j.properties");
     properties.setProperty("log4j.appender.allConsoleLog.layout", "org.apache.log4j.PatternLayout");
     properties.setProperty("log4j.appender.allConsoleLog.layout.ConversionPattern", "%-5p: [%t] %C.%M():%3L %m%n");
     properties.setProperty("log4j.appender.allConsoleLog", "org.apache.log4j.ConsoleAppender");
    
     properties.store(
         fileOutputStream,"No comment");

     fileOutputStream.close();
}

public static void name() {
	List li= new ArrayList();
	li.add("boofcv-feature-0.41.jar");
	li.add("boofcv-geo-0.41.jar");
	li.add("boofcv-io-0.41.jar");
	li.add("boofcv-ip-multiview-0.41.jar");
	li.add("boofcv-recognition-0.41.jar");
	li.add("boofcv-sfm-0.41.jar");
	li.add("boofcv-swing-0.41.jar");
	li.add("boofcv-types-0.41.jar");
	li.add("ddogleg-0.22.jar");
	li.add("georegression-0.25.jar");
	li.add("boofcv-all-0.41.jar");
	li.add("boofcv-core-0.41.jar");
	li.add("visualize-0.26.jar");
	genCode(li);

}
public static void genCode(List li) {
	for (int i = 0; i < li.size(); i++) {
		String [] splitItem= ObjectUtils.toString(li.get(i),"").split("-");
		String version=splitItem[splitItem.length-1].replace(".jar", "");
		String nameLib= ObjectUtils.toString(li.get(i),"").replace("-"+version+".jar", "");
		String mavevStr="<dependency>\r\n" + 
				"			<groupId>org.boofcv</groupId>\r\n" + 
				"			<artifactId>"+nameLib+"</artifactId>\r\n" + 
				"			<version>"+version+"</version>\r\n" + 
				"			<scope>system</scope>\r\n" + 
				"			<systemPath>${project.basedir}/src/main/webapp/WEB-INF/lib/"+ObjectUtils.toString(li.get(i),"")+"\r\n" + 
				"			</systemPath>\r\n" + 
				"</dependency>";
		System.out.println(mavevStr);
	}
}
}
