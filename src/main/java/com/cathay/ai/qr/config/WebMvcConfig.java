package com.cathay.ai.qr.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Configuration
@EnableWebMvc
public class WebMvcConfig extends WebMvcConfigurerAdapter {

 
 
   // Cấu hình để sử dụng các file nguồn tĩnh (html, image, ..)
   @Override
   public void addResourceHandlers(ResourceHandlerRegistry registry) {
       registry.addResourceHandler("/css/**").addResourceLocations("/WEB-INF/public/css/").setCachePeriod(31556926);
       registry.addResourceHandler("/img/**").addResourceLocations("/WEB-INF/public/img/").setCachePeriod(31556926);
       registry.addResourceHandler("/js/**").addResourceLocations("/WEB-INF/public/js/").setCachePeriod(31556926);
       registry.addResourceHandler("/controller/**").addResourceLocations("/WEB-INF/public/controller/").setCachePeriod(31556926);
       registry.addResourceHandler("/webfonts/**").addResourceLocations("/WEB-INF/public/webfonts/").setCachePeriod(31556926);
       registry.addResourceHandler("/uploads/**").addResourceLocations("/").setCachePeriod(31556926);
       registry.addResourceHandler("/layout/**").addResourceLocations("/").setCachePeriod(31556926);
   }

   
   @Override
   public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
	   System.out.println("vo ");
	  System.out.println(configurer);
	   configurer.enable();
       
   }

}