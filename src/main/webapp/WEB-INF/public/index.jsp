<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!doctype html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>ApiQRcode</title>

<%@include file="layout/link.jsp"%>
<%
	String strContextPath = request.getContextPath();
%>
</head>

<body>
	<a class="skippy sr-only sr-only-focusable" href="#content"> <span
		class="skippy-text">Trở về nội dung chính</span>
	</a>


	<%@include file="layout/header.jsp"%>

	<div class="container-fluid">
		<div class="row flex-xl-nowrap">
			<%@include file="layout/nav.jsp"%>
			<main class="col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content"
				role="main">
				<h1 class="bd-title" id="content">Nội Dung</h1>
				<p>Tổng quan hệ thống API</p>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th scope="col">Java</th>
							<th scope="col">Tomcat</th>
							<th scope="col">JavaWebSring</th>
							<th scope="col">Boofcv</th>

						</tr>
					</thead>
					<tbody>
						<tr>
							<td scope="row">
								<div class="text-success">
									Phiên bản mới nhất
									<code class="font-weight-normal text-nowrap">(javaVersion:
										[17.0.5])</code>
								</div>
							</td>
							<td class="text-success"><code
									class="font-weight-normal text-nowrap">9</code></td>
							<td class="text-success">Spring JPA</td>
							<td class="text-success">0.40.1</td>
						</tr>
					</tbody>
				</table>
				<center>
					<img class="img-fluid" src="<%=strContextPath%>/img/download.png" />
				</center>
				<h2 id="css-files">
					<span class="bd-content-title">Giải mã file hình ảnh QRcode<a
						class="anchorjs-link " aria-label="Anchor" data-anchorjs-icon="#"
						href="#css-files" style="padding-left: 0.375em;"></a></span>
				</h2>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th scope="col">Đường dẫn</th>
							<th scope="col">Phương thức</th>
							<th scope="col">Tham số truyền vào</th>
							<th scope="col">Kết quả trả về</th>

						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row">
								<div>
									<code class="font-weight-normal text-nowrap">
										http://27.64.26.210:8080<%=strContextPath%>/qrcode
									</code>
								</div>
							</th>
							<td class="text-warning">POST</td>
							<td class="text-success">{file:qrcode.png}</td>
							<td class="text-success">JSON</td>
						</tr>
						<tr>
							<th scope="row">
								<div>
									<code class="font-weight-normal text-nowrap">
										http://27.64.26.210:8080<%=strContextPath%>/qrcodeBase64
									</code>
								</div>
							</th>
							<td class="text-warning">POST</td>
							<td class="text-success">{imageString:qrcode.png}</td>
							<td class="text-success">JSON</td>
						</tr>
					</tbody>
				</table>
				<h2 id="css-files">
					<span class="bd-content-title">Giải mã file hình ảnh File
						tiff (Barcode)<a class="anchorjs-link " aria-label="Anchor"
						data-anchorjs-icon="#" href="#css-files"
						style="padding-left: 0.375em;"></a>
					</span>
				</h2>
				<table class="table table-bordered">
					<thead>
						<tr>
							<th scope="col">Đường dẫn</th>
							<th scope="col">Phương thức</th>
							<th scope="col">Tham số truyền vào</th>
							<th scope="col">Kết quả trả về</th>

						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row">
								<div>
									<code class="font-weight-normal text-nowrap">
										http://27.64.26.210:8080<%=strContextPath%>/barcodetiff
									</code>
								</div>
							</th>
							<td class="text-warning">POST</td>
							<td class="text-success">{file:barcode.tiff}</td>
							<td class="text-success">JSON</td>
						</tr>
					</tbody>
				</table>
				<h2 id="css-files">
					<span class="bd-content-title">Tạo mã QRCode </span>
				</h2>
				<h2 id="css-files">
					<span class="bd-content-title">Tạo mã Barcode </span>
				</h2>
			</main>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
	<script>
		window.jQuery
				|| document
						.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')
	</script>
	<script
		src="https://getbootstrap.com/docs/4.4/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/docsearch.js@2/dist/cdn/docsearch.min.js"></script>
	<script src="https://getbootstrap.com/docs/4.4/assets/js/docs.min.js"></script>


</body>

</html>