# JavaQRCode

## 🔲 API Nhận diện QR Code

### Yêu Cầu
-      Java Phiên Bản Mới Nhất
-      Tomcat 9
-   `http://171.227.221.64:8080/JavaQrCode` - try it
Đường dẫn API

-   `http://171.227.221.64:8080/JavaQrCode/qrcode` — parameter :`file`.
or
-   `http://webdano.ddns.net:8080/JavaQrCode/qrcode` — parameter :`file`.
    
-   `http://171.227.221.64:8080//JavaQrCode/qrcodeBase64` — parameter :`imageString`.
or
-   `http://webdano.ddns.net:8080/JavaQrCode/qrcodeBase64` — parameter :`imageString`.
## Follow Me
-      😊 GitLab: https://gitlab.com/doanmeo202
-      📚 Website: https://webdano.com 
-      ✨ 🥤 Cảm ơn bạn đã ghé qua!  ✨
